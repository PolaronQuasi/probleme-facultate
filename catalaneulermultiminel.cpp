#include <iostream>
using namespace std;
int Factorial(int n) {
   long factorial = 1;
   int i = 1;
   while (i <= n) {
      factorial = factorial * i;
      i = i + 1;
   }
   return factorial;
}
int Putere(int n, int m) {
   int p = 1;
   while (m != 0)

   {
      p = p * n;
      m = m - 1;
   }
}
int Prim(int n) {
   if (n < 2) return 0;
   if (n == 2) return 1;
   for (int i = 2; i <= n / 2; i++)
      if (n % i == 0) return 0;
   return 1;
}

int Combinari(int n, int k) {
int aux;
if(k>n)
{
aux=n;
n=k;
k=aux;
}
return Factorial(n) / (Factorial(k) * Factorial(n - k));
}
float Catalan(int n) {
   return Combinari(2 * n, n) / (float)(n + 1);
}
int VImpartire(int n, int m) {
   if ((n % m) != 0) return 1;
   return 0;
}

int Euler(int n,int &contor) {
   contor = 0;
   cout << "Numerele prime cu numarul dat si mai mici ca el sunt: ";
   for (int i = 1; i <= n; i++) {
      if ((i == 1) || (Prim(i) == 1) && (VImpartire(n, i) == 1)) {
         cout << i << " ";
         contor = contor + 1;
      }
   }
}


int main() {
int VariabilaDeAles;
cout<<"Tastati 1 pentru numarul lui Catalan"<<endl;
cout<<"Tastati 2 pentru numar relatii pe multime n elemente"<<endl;
cout<<"Tastati 3 pentru numar Euler"<<endl;
cin>>VariabilaDeAles;
switch(VariabilaDeAles)
{
   case 1:
      cout << "Ati ales calculul numarului lui Catalan\n";
      cout << "Introduceti numarul: " << endl;
      int A;
      cin >> A;
      cout<<"\n\n\n\n";
      cout << "Numarul lui Catalan este : " << Catalan(A);
      break;
   case 2:
      cout << "Ati ales numarul de relatii pe o multime cu n elemente\n";
      cout << "Introduceti numarul de elemente " << endl;
      cin >> A;
      cout<<"\n\n\n\n";
      cout << "Numarul de relatii pe o multime cu n elemente este : " << Putere(2, A * A);
      break;
   case 3:
      cout << "Ati ales calculul indicatorului lui Euler\n";
      cout << "Introduceti numarul : " << endl;
      cin >> A;
      cout<<"\n\n\n\n";
      int contor =0;
      cout << "Numarul de relatii pe o multime cu n elemente este : " << Euler(A,contor);
      cout << "\nIndicatorul lui Euler este: " << contor+1 << endl;
      break;

}
}
