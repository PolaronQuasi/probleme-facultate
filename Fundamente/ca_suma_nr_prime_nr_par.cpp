#include <iostream>
using namespace std;

int verificareNrPrim(int n) {
  if (n < 2) return 0;
  if (n == 2) return 1;
  for (int i = 3; i * i < n; i = i + 2)
    if (n % i == 0) return 0;
  return 1;
}
void descopunerenumarinnrprime(int n) {
  int ultimulnrprim = 3; // Ultinul nr prim este 3, 2 este valabil in cazul lui 4
  while (ultimulnrprim <= n / 2) //Daca ultimul numar prim nu a depasit jumatatea sirului
  {
    if (verificareNrPrim(ultimulnrprim) == 1 && verificareNrPrim(n - ultimulnrprim) == 1) {
      cout << n << " = " << ultimulnrprim << " + ";
      cout << n - ultimulnrprim << endl; //Se verifica daca n-ultimulnrprim si daca ultimul numar prim este prim
    }
    ultimulnrprim = ultimulnrprim + 2; // Numerele prime se pot aduna cu multipli de 2 pentru a da alte numere prime
  }

}

int main() {
  int n;
  cout << "n=";
  cin >> n;
  if (n == 4) cout << "2 + 2";
  else if (n % 2 != 0) cout << "Numarul dat nu este par" << endl;
  else descopunerenumarinnrprime(n);
  return 0;
}