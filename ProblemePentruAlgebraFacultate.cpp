#include <iostream>

#include <stdlib.h>

using namespace std; //Codul functiilor poate fi regasit dupa functia main.
long rezultatCombinari(long n, long k); //Calculeaza Combinari de n luate cate k
long calculFactorial(int n); //Calculeaza Factorialul
void stergereEcran(); //"Sterge ecranul" , nu am folosit system("CLS"), deoarece acesta nu exista pe linux.(compatibilitate)
long rezultatAranjamente(int A, int B); //Calculeaza Aranjamente de n luate cate k
long calculPutere(int n, int m); //Calculeaza n la puterea m
long numarulFunctiilorSurjective(int n, int m); //Calculeaza numarul functiilor Surjective
long calculNumarFunctiiMonotone(int n, int m); //Calculeaza numarul functiilor Monotone
float calcululNumaruluiCatalan(int n); //Calculeaza numarul lui Catalan , am folosit formula Combinari de 2n luate cate n impartit la n+1 = Catalan(n)
long indicatorulLuiEuler(int n); // Calculeaza indicatorul lui Euler
long verificareNumarPrim(int n); //Verifica primalitate numar;
long functieTest(int n, int m); //functie care testeaza functionalitatea utilizata pentru calcularea indicatorului lui Euler...
long calculCMMDC(int n, int m); //functie care calculeaza cel mai mic divizor comun
void diferentamultimi(int A[100],int B[100],int &n,int &m); //Face diferenta dintre 2 multimi
void citireMatrice(int N[100],int M[100],int &n,int &m);//Citeste doua matrici , pentru diferenta submultimilor
void intersectiemultimi(int N[100], int M[100], int &n, int &m);//Functie care realizeaza intersecia
void reuniunemultimi(int N[100], int M[100], int &n, int &m);//Functie care efectueaza reuniunea
int main() {
   int optiune;
   reset:
      stergereEcran();
   cout << "Apasati tasta 1 pentru calculul numarului functiilor strict crescatoare\n";
   cout << "Apasati tasta 2 pentru calculul numarului functiilor injective\n";
   cout << "Apasati tasta 3 pentru calculul numarului functiilor surjective\n";
   cout << "Apasati tasta 4 pentru calculul numarului functiilor monotone\n";
   cout << "Apasati tasta 5 pentru calculul numarului lui Catalan\n";
   cout << "Apasati tasta 6 pentru calculul numarul de relatii pe o multime cu n elemente\n";
   cout << "Apasati tasta 7 pentru calculul indicatorul lui Euler\n";
   cout << "Apasati tasta 8 pentru afisarea subgrupurilor lui Zn\n";
   cout << "Apasati tasta 9 pentru operatia de diferenta a doua multimi\n";
   cout << "Apasati tasta 10 pentru efectuarea intersectiei a doua multimi\n";
   cout << "Apasati tasta 11 pentru efectuarea reuniunii a doua multimi\n";

   cin >> optiune;
   switch (optiune) {
   case 1:
      cout << "Ati ales calculul numarului functiilor strict crescatoare\n";
      int A, B;
      cout << "Introduceti cardinalul multimii A: " << endl;
      cin >> A;
      cout << "Introduceti cardinalul multimii B: " << endl;
      cin >> B;
      stergereEcran();
      if (A <= B)
         cout << "Numarul functiilor strict crescatoare este : " << rezultatCombinari(B, A);
      else
         cout << "Numarul functiilor strict crescatoare este : " << rezultatCombinari(A, B);
      break;
   case 2:
      cout << "Ati ales calculul numarului functiilor injective\n";
      cout << "Introduceti cardinalul multimii A: " << endl;
      cin >> A;
      cout << "Introduceti cardinalul multimii B: " << endl;
      cin >> B;
      stergereEcran();
      if (A <= B)
         cout << "Numarul functiilor injective este : " << rezultatAranjamente(B, A);
      else
         cout << "Numarul functiilor injective este : " << rezultatAranjamente(A, B);
      break;
   case 3:
      cout << "Ati ales calculul numarului functiilor surjective\n";
      cout << "Introduceti cardinalul multimii A: " << endl;
      cin >> A;
      cout << "Introduceti cardinalul multimii B: " << endl;
      cin >> B;
      stergereEcran();
      if (A <= B)
         cout << "Numarul functiilor surjective este : " << numarulFunctiilorSurjective(B, A);
      else
         cout << "Numarul functiilor surjective este : " << numarulFunctiilorSurjective(A, B);
      break;
   case 4:
      cout << "Ati ales calculul numarului functiilor monotone\n";
      cout << "Introduceti cardinalul multimii A: " << endl;
      cin >> A;
      cout << "Introduceti cardinalul multimii B: " << endl;
      cin >> B;
      stergereEcran();
      if (A <= B)
         cout << "Numarul functiilor monotone este : " << calculNumarFunctiiMonotone(B, A);
      else
         cout << "Numarul functiilor monotone este : " << calculNumarFunctiiMonotone(A, B);
      break;
   case 5:
      cout << "Ati ales calculul numarului lui Catalan\n";
      cout << "Introduceti numarul: " << endl;
      cin >> A;
      stergereEcran();
      cout << "Numarul lui Catalan este : " << calcululNumaruluiCatalan(A);
      break;
   case 6:
      cout << "Ati ales numarul de relatii pe o multime cu n elemente\n";
      cout << "Introduceti numarul de elemente " << endl;
      cin >> A;
      stergereEcran();
      cout << "Numarul de relatii pe o multime cu n elemente este : " << calculPutere(2, A * A);
      break;
   case 7:
      cout << "Ati ales calculul indicatorului lui Euler\n";
      cout << "Introduceti numarul : " << endl;
      cin >> A;
      stergereEcran();
      cout << "Numarul de relatii pe o multime cu n elemente este : " << indicatorulLuiEuler(A);
      break;
   case 8:
      cout << "Ati ales afisarea subgrupurilor lui Zn\n";
      cout << "Introduceti n : " << endl;
      int n, i, j;
      cin >> n;
      stergereEcran();
      cout << "Subgrupurile sunt: ";
      for (i = n; i > 0; i--) {
         if (calculCMMDC(n, i) != 1 || (i == 1)) {
            for (j = n; j >= 0; j = j - i)
               if (((j < n) || (calculCMMDC(n, j) != 1)) && (i <= n / 2))
                  if (j < n) {
                     cout << j;
                     cout << " ";
                  }
         }
         if (calculCMMDC(n, i) != 1 || (i == 1))
            cout << endl;
      }
      break;
   case 9:
      cout << "Ati ales diferenta a doua multimi\n";
      int N[100],M[100],m;
      citireMatrice(N,M,n,m);
      stergereEcran();
      cout<<"Diferenta multimilor este: \n";
      diferentamultimi(N,M,n,m);
      break;

   case 10:
      cout << "Ati ales reuniunea a doua multimi\n";
      citireMatrice(N,M,n,m);
      stergereEcran();
      cout<<"Reuniunea multimilor este: \n";
      reuniunemultimi(N,M,n,m);
      break;

   case 11:
      cout << "Ati ales intersectia a doua multimi\n";
      citireMatrice(N,M,n,m);
      stergereEcran();
      cout<<"Intersectia multimii este: \n";
      intersectiemultimi(N,M,n,m);
      break;
   }

   cout << "\n\n\nProgram finalizat/Programul cu numarul inserat nu exista.\n";
   cout << "\n\n\nApasati 1 ca sa resetati programul\nApasati alta tasta ca sa inchideti programul." << endl;
   cin >> optiune;
   if (optiune == 1)
      goto reset;
}
void intersectiemultimi(int N[100], int M[100], int &n, int &m)  {
   int contor;
   for (int i = 0; i < n; i++) {
      contor = 0;
      for (int j = 0; j < m; j++)
         if (N[i] == M[j])
            contor++;
      if (contor != 0)
         cout << N[i] << " ";
   }
}
void reuniunemultimi(int N[100], int M[100], int &n, int &m)  {
   diferentamultimi(N, M, n, m);
   for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++)
         if (N[i] == M[j])
            cout << N[i] << " ";
   }
   diferentamultimi(N, M, n, m);
}
void citireMatrice(int N[100], int M[100], int &n, int &m) {
   cout << "Introduceti dimensiunea multimii A." << endl;
   cin >> n;
   cout << "Introduceti pe rand elementele multimii A: " << endl;
   for (int i = 0; i < n; i++)
      cin >> M[i];
   cout << "Introduceti dimensiunea multimii B." << endl;
   cin >> m;
   cout << "Introduceti pe rand elementele multimii B: " << endl;
   for (int i = 0; i < m; i++)
      cin >> N[i];

}
void diferentamultimi(int N[100], int M[100], int &n, int &m) {
   int contortrei;
   for (int i = 0; i < n; i++) {
      contortrei = 0;
      for (int j = 0; j < m; j++)
         if (N[i] == M[j])
            contortrei++;
      if (contortrei == 0)
         cout << N[i] << " ";
   }

}
long calculCMMDC(int n, int m) {
   while (m != n) {
      if (n < m) m = m - n;
      else n = n - m;
   }
   return n;
}
long verificareNumarPrim(int n) {
   if (n < 2) return 0;
   if (n == 2) return 1;
   for (int i = 2; i <= n / 2; i++)
      if (n % i == 0) return 0;
   return 1;
}
long functieTest(int n, int m) {
   if ((n % m) != 0) return 1;
   return 0;
} //1 5 7 11

long indicatorulLuiEuler(int n) {
   int contor = 0;
   cout << "Numerele prime cu numarul dat si mai mici ca el sunt: ";
   for (int i = 1; i <= n; i++) {
      if ((i == 1) || (verificareNumarPrim(i) == 1) && (functieTest(n, i) == 1)) {
         cout << i << " ";
         contor = contor + 1;
      }
   }
   cout << "\nIndicatorul lui Euler este: " << contor << endl;
}
float calcululNumaruluiCatalan(int n) {
   return rezultatCombinari(2 * n, n) / (float)(n + 1);
}
long calculNumarFunctiiMonotone(int n, int m) {
   return 2 * rezultatCombinari(n, m);
}
long numarulFunctiilorSurjective(int n, int m) {

   for (int i = 0; i <= n; i++) {

      int a, b, c, rezultat;
      a = calculPutere(-1, i);
      c = rezultatCombinari(m, i);
      b = calculPutere(m - i, n);
      rezultat = rezultat + (a * c * b);
   }
}
long rezultatAranjamente(int n, int k) {
   return calculFactorial(n) / calculFactorial(n - k);
}
long calculPutere(int n, int m) {
   int p = 1;
   while (m != 0)

   {
      p = p * n;
      m = m - 1;
   }
}
long calculFactorial(int n) {
   long factorial = 1;
   int i = 1;
   while (i <= n) {
      factorial = factorial * i;
      i = i + 1;
   }
   return factorial;
}
long rezultatCombinari(long n, long k) {
   return calculFactorial(n) / (calculFactorial(k) * calculFactorial(n - k)); //Este folosita formula matematica;
}
void stergereEcran() {
   int i;
   for (i = 0; i < 10; i++)
      cout<<"\n\n\n";
}
