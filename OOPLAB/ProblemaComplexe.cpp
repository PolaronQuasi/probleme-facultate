#include <iostream>
#include <math.h>
using namespace std;

class Complex
{
private:
    double* _re;
    double* _im;
public:
    //constructori
    Complex();
    Complex(double, double);
    Complex(const Complex&);
    //afisare, citire consola
    void Display();
    void Read();
    //metode get si set
    double GetRe();
    void SetRe(double);
    double GetIm();
    void SetIm(double);
    //operatii
    double Modul();
    //destructor
    ~Complex();
Complex Suma(const Complex& a)
{
        cout<<"A fost apelata functia Suma"<<endl;
        Complex b = a ;
        cout<<"Numarul este : " << *_re+b.GetRe() << "+" << *_im+b.GetIm()<<"i"<<endl;
}
Complex Produs(const Complex& a)
{
        cout<<"A fost apelata functia Produs"<<endl;
        Complex b = a ;
        cout<<"Numarul este : " << *_re*b.GetRe() << "+" << *_im*b.GetIm()<<"i"<<endl;
}
Complex Diferenta(const Complex& a)
{
        cout<<"A fost apelata functia Diferenta"<<endl;
        Complex b = a ;
        cout<<"Numarul este : " << *_re-b.GetRe() << "+" << *_im-b.GetIm()<<"i"<<endl;

}
Complex Suma(double a)
{
    cout<<"A fost apelata functia Modul"<<endl;
    cout<<"Numarul este : " << *_re+a << "+" << *_im<<"i"<<endl;
}
};

//implementare (testare in functia main)
Complex::Complex()
{
    _re = new double (0);
    _im = new double (0);
    cout<<"Constructorul default a fost apelat"<<endl;
}
Complex::Complex(double a, double b)
{
    _re = new double (a);
    _im = new double (b);
    cout<<"Constructorul cu parametrii (double , double ) a fost apelat"<<endl;
}
Complex::Complex(const Complex& a)
{
    Complex b = a ;
    _re = new double (*b._re);
    _im = new double (*b._im);
    cout<<"Constructorul cu parametrii (const Complex& ) a fost apelat"<<endl;
}
Complex::~Complex()
{
    delete _re;
    delete _im;
    cout<<"A fost apelat destructorul"<<endl;
}
void Complex::Display()
{
    cout<<"A fost apelata functia display"<<endl;
    cout<<"Numarul este : " << *_re << "+" << *_im<<"i"<<endl;
}
void Complex::Read()
{
    cout<<"A fost apelata functia read"<<endl;
    int temp1,temp2;
    cin>>temp1;
    cin>>temp2;
    _re = new double (temp1);
    _im = new double (temp2);
    Display();
}
double Complex::GetRe()
{
    cout<<"A fost apelata functia GetRe"<<endl;
    return *_re;
}
void Complex::SetRe(double a)
{
    cout<<"A fost apelata functia SetRe"<<endl;
    _re = &a;
}
double Complex::GetIm()
{
    cout<<"A fost apelata functia GetIm"<<endl;
    return *_im;
}
void Complex::SetIm(double a)
{
    cout<<"A fost apelata functia SetIm"<<endl;
    _im = &a;
}
double Complex::Modul()
{
    cout<<"A fost apelata functia Modul"<<endl;
    cout<<"Modulul este: ";
    return sqrt((*_re**_re)+(*_im**_im));
}
int main()
{

    Complex * constDefault = new Complex();
    Complex * doubledouble = new Complex(2.0,3.0);
    Complex b(2,3);
    //Complex * complexa = new Complex(b);
    doubledouble->Display();
    doubledouble->Read();
    cout<<doubledouble->GetIm()<<endl;
    cout<<doubledouble->GetRe()<<endl;
    doubledouble->SetRe(3);
    doubledouble->Display();
    doubledouble->SetIm(2);
    doubledouble->Display();
    cout<<doubledouble->Modul();
    doubledouble->Suma(b);
    doubledouble->Produs(b);
    doubledouble->Diferenta(b);
    doubledouble->Suma(2);
    return 0;
}
