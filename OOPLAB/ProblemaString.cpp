#include <iostream>
#include <string.h>
using namespace std;

class MyString
{
  private:
        char* _text; //caracterele alocare dinamica in constructori (nu uitati de terminatorul de siruri!!)
        int _len; //lungimea textului
  public:
        MyString(); //textul vid (doar terminatorul de sir!!; _len este zero dar se aloca un spatiu pentru '\0'!!)
        MyString(char*); //se copiaza caracterele din parametru; se aloca spatiu si se initializeaza _len
        MyString(const MyString&);//copiere
        //afisare la consola
        void Display();
        //metode get si set
        char GetAt(int); //intoarce caracterul de pe pozitia data ca parametru
        char* GetText(); //toate caracterele
        void SetAt(int, char);//schimba caracterul de pe pozitia transmisa ca parametru
        //operatii
        MyString Concat(const MyString&);//concatenare
        MyString Concat(char);//concatenarea cu un caracter
        MyString Concat(int);//MyString m("abc"); m.Concat(5) ==> "abc5"
       //8 MyString& ToUpper();//transformarea tuturor caracterelor in majuscule
        //atentie, se modifica obiectul curent (*this)
        //ToUpper returneaza *this (obiectul curent)
        MyString& Trim(char); //elimina toate aparitiile
        //caracterului transim ca parametru din sirul curent
        //(din nou, se modifica obiectul, deci returnam *this)
        //comparare
        bool Equals(const MyString&); //se verifica daca
        //cele doua siruri contin aceleasi caractere!!
        MyString& ToUpper()
           {
             for(int i=0;i<_len;i++)
              _text[i]=toupper(_text[i]);
            }

        ~MyString(); //destructor
};
bool MyString::Equals(const MyString& a)
{
    MyString b=a;
    if(strcmp(_text,b.GetText()))
    return false;
    else return true;
}
MyString::MyString()
{
  _len=1;
  _text = new char[_len];
  _text[0]=0;
}
MyString::MyString(char * a)
{
    _len=strlen(a);
    _text = new char[_len];
    strcpy(_text,a);
}
MyString::MyString(const MyString& a)
{
    MyString b=a;
    _len = b._len;
    _text = new char[_len];
    strcpy(_text,b._text);
    ///cout<<_text<<"--"<<endl;
}
MyString::~MyString()
{
    _len=0;
    delete[] _text;
}
void MyString::Display()
{
  for(int i = 0 ; i < _len ; i++)
        cout<<_text[i];
  cout<<"\n\n";
}
char MyString::GetAt(int a)
{
    return _text[a];
}
char* MyString::GetText()
{
    return _text;
}
void MyString::SetAt(int a, char b)
{
    _text[a]=b;
}
MyString MyString::Concat(int a)
{
    _len++;
    _text[_len-1]= a +'0';
}
MyString MyString::Concat(char a)
{
    _text[_len++]=a;
    _text[_len]=0;
}
MyString& MyString::Trim(char c)
{
    //numaram cate caractere c sunt in text!!
    if(_len == 0) //nu avem ce numara!
    {
        return *this;
    }

    int count = 0;
    for(int i = 0; i <= _len; i++)
    {
        if(_text[i] == c)
        {
            count++;
        }
    }

    if(count == 0)
    {
        return *this;
    }

    //alocam noul spatiu!!
    char* temp = new char[_len - count + 1];
    //copiem caracterele
    int k = 0;
    for(int i = 0; i <= _len; i ++)
    {
        if(_text[i] != c)
        {
          temp[k++] = _text[i];
        }
    }

    //eliberam vechiul spatiu!!!
    delete[] _text;
    _len = _len - count;
    //reinitializam:
    _text = temp;
    return *this;
}
int main()
{
    char *c="abc";
    MyString b(c),d("robo");
    b.SetAt(0, 'a');
    b.Display();
    b.Concat('a');
    b.Display();
    //d.Concat(b);
    d.Display();
   cout<< b.GetText();
    cout<< d.GetText();
    //testare in main
    //MyString m("adb5 dfd8");
    //m.Trim('d'); //realocarea spatiului!!!
    //m.Display(); //ab5 f8

    return 0;
}
