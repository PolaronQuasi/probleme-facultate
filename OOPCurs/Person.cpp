#include "Person.h"
#include <iostream> // nu este practica buna ...
using namespace std;
Person::Person() //constructor default
{
    nume = "Nedefinit";
    varsta = -1;
}

Person::~Person() //destructor default
{
}

Person::Person(char *nm, int var)  //constructor 2 parametrii
{
    nume=nm;
    varsta=var;
}

Person& Person::operator+ (Person & DariusNicolae) {
	//Andrei s-a intalnit cu Sara
   Person temp = DariusNicolae;
   cout<<this->Getnume()<<" s-a intalnit cu " << temp.Getnume();
   cout<<"\n";
   return DariusNicolae;
}
Person Person::operator+(int valoare)
{
    this->Setvarsta(this->Getvarsta()+valoare); // am stat varsta + n;

	return *this;
}

/*
Person& operator+ (const Person& DariusNicolae2, int n) { // nu am gasit o metoda
    Person DariusNicolae = DariusNicolae2;
    DariusNicolae.Setvarsta(DariusNicolae.Getvarsta()+n); // am stat varsta + n;
    return DariusNicolae;
}
Person& operator+ (int n, const Person& DariusNicolae2) { // nu am gasit o metoda
    Person DariusNicolae = DariusNicolae2;
    DariusNicolae.Setvarsta(DariusNicolae.Getvarsta()+n); // am stat varsta + n;
    return DariusNicolae;
}
*/


