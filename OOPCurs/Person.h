#ifndef PERSON_H
#define PERSON_H
#include <iostream>
using namespace std;

class Person
{
public:
    Person();
    Person(char *nm, int var);
    virtual ~Person();
    char * Getnume()
    {
        return nume;
    }
    void Setnume(char * val)
    {
        nume = val;
    }
    int Getvarsta()
    {
        return varsta;
    }
    void Setvarsta(int val)
    {
        varsta = val;
    }
    Person& operator+ (Person & Darius);
    Person operator+(int valoare);
    //friend Person& operator+ (int n,Person& DariusNicolae);
    //friend Person& operator+ (Person& DariusNicolae,int n);
    //friend Person& operator+=(const int& varsta);




private:
    char * nume;
    int  varsta;
};

#endif // PERSON_H
